"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""
import os
import flask
from flask import request
from pymongo import MongoClient
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
from flask_restful import Resource, Api

import logging

###
# Globals
###
app = flask.Flask(__name__)
api = Api(app)
client = MongoClient("172.18.0.2", 27017)

db = client.tododb
CONFIG = config.configuration()
app.secret_key = "SECRET KEY"

db_array=[]
###
#APIs
###

class listAll(Resource):
    def get(self):
        _items = db.tododb.find()
        items = [item for item in _items]
        timeItems = []
        for item in items:
            timeItems.append(item['open'] + " " + item['close'])
        return {"time":timeItems}



class listAllJSON(Resource):
    def get(self):
        _items = db.tododb.find()
        items = [item for item in _items]
        timeItems=[]
        for item in items:
            timeItems.append(item['open'] + " " + item['close'])
        return {"time":timeItems}

class listOpenOnly(Resource):
    def get(self):
        _items = db.tododb.find()
        app.logger.debug("From database: {}".format(_items))
        items = [item for item in _items]
        timeItems = []
        for item in items:
            timeItems.append(item['open'])
        return {"time":timeItems}

class listOpenOnlyJSON(Resource):
    def get(self):
        _items = db.tododb.find()
        items = [item for item in _items]
        timeItems = []
        top = flask.request.args.get('top')
        if (top == None) or (int(top)>len(items)):
            top = len(items)
        for i in range(int(top)):
            timeItems.append(items[i]['open'])
        return {"time":timeItems}
        

class listCloseOnly(Resource):
    def get(self):
        _items = db.tododb.find()
        app.logger.debug("From database: {}".format(_items))
        items = [item for item in _items]
        timeItems = []
        for item in items:
            timeItems.append(item['close'])
        return {"time":timeItems}

class listCloseOnlyJSON(Resource):
    def get(self):
        _items = db.tododb.find()
        items = [item for item in _items]
        timeItems = []
        top = flask.request.args.get('top')
        if (top == None) or (int(top)>len(items)):
            top = len(items)

        for i in range(int(top)):
            timeItems.append(items[i]['close'])
        return {"time":timeItems}


class listOpenOnlyCSV(Resource):
    def get(self):
        _items = db.tododb.find()
        app.logger.debug("From database: {}".format(_items))
        items = [item for item in _items]
        itemStr = ""
        top = flask.request.args.get('top')
        if (top == None) or (int(top)>len(items)):
            top = len(items)
        for i in range(int(top)):
            itemStr = itemStr + str(items[i]['open']) + ", "
        return itemStr


class listCloseOnlyCSV(Resource):
    def get(self):
        _items = db.tododb.find()
        app.logger.debug("From database: {}".format(_items))
        items = [item for item in _items]
        itemStr = ""
        top = flask.request.args.get('top')
        if (top == None) or (int(top)>len(items)):
            top = len(items)
        for i in range(int(top)):
            itemStr = itemStr + str(items[i]['close']) + ", "
        return itemStr


class listAllCSV(Resource):
    def get(self):
        _items = db.tododb.find()
        app.logger.debug("From database: {}".format(_items))
        items = [item for item in _items]
        itemStr = ""
        for item in items:
            itemStr = itemStr + str(item['open'])+ ", " + str(item['close']) + ", "
        return itemStr


api.add_resource(listAll, '/listAll')
api.add_resource(listOpenOnly, '/listOpenOnly')
api.add_resource(listCloseOnly, '/listCloseOnly')
api.add_resource(listAllCSV, '/listAll/csv')
api.add_resource(listAllJSON, '/listAll/json')
api.add_resource(listOpenOnlyJSON, '/listOpenOnly/json')
api.add_resource(listCloseOnlyJSON, '/listCloseOnly/json')
api.add_resource(listCloseOnlyCSV, '/listCloseOnly/csv')
api.add_resource(listOpenOnlyCSV, '/listOpenOnly/csv')



###
# Pages
###

@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')

@app.route('/sub')
def sub():
    app.logger.debug("Entered submit function")
    app.logger.debug("db_array: {}".format(db_array))
    for item in db_array:
        db.tododb.insert_one(item)
    return "nothing"

@app.route("/disp")
def display():
    app.logger.debug("Entered display function")
    _items = db.tododb.find()
    app.logger.debug("From database: {}".format(_items))
    items = [item for item in _items]
    app.logger.debug("Printing items: {}".format(items))
    return flask.render_template('todo.html', items=items)
 

@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    distance = request.args.get('distance', 200, type=float)
    beginDate = request.args.get('begindate', str)
    beginTime = request.args.get('begintime', str)
    beginDate = beginDate + " " + beginTime
    app.logger.debug("km={}".format(km))
    app.logger.debug("dist={}".format(distance))
    app.logger.debug("time={}".format(beginDate))

    app.logger.debug("request.args: {}".format(request.args))
    open_time = acp_times.open_time(km, distance, beginDate)
    close_time = acp_times.close_time(km, distance, beginDate)
    item_dict = {"km": km, "distance": distance, "beginDate": beginDate, "open":open_time, "close": close_time  }
    app.logger.debug("item_dict: {}".format(item_dict))
    db_array.append(item_dict)
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)


#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(host="0.0.0.0", debug=True)
